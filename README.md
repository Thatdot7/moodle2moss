# Moodle2Moss

This script takes a set of Moodle assignment submissions and submits particular files to Moss for Plagiarism checking.

## Requirements
Either:
  - Windows with Linux subsystem installed
  - Linux (Ubuntu)
  - macOS

Script requires the following command line utilities:
  cut, tail, unzip, find, sed, basename, grep, perl

## Usage Instructions
1.  Apply for Moss account and download the 'moss.pl' script.  This script needs to be in your path.
2.  Update this script with required settings at the top.
3.  Go to the Moodle assignment submission, click on "View/grade all submissions.  Under "Grading actions" select "Download all submissions".
4.  Unzip the submissions zip file and change into the resulting directory.
5.  Create a folder called "template" in the resulting directory and insert your skeleton files in that folder.
6.  Open up `moss.pl` and change the `$userid` variable and insert your own userid (that you would had retrieved from Stanford).
5.  Open up `runmoss.sh` and change the `ASSIGNMENTFILES` variable with a list of the filenames of the template files with filenames delimited by a space.
6.  Run `./runmoss.sh extract` to unzip all the submissions and put the assignments files found within into the 'ForMoss' directory.
  - If after running `./runmoss.sh extract`, you have some errors. You may go back and reextract the failed submissions in the entity folder without the "_assignsubmission_file_" in the folder name then run `./runmoss.sh rebuild` to fix the 'ForMoss' folder with the new changes.
7.  Run `./runmoss.sh moss` in that directory to submit the submissions for each assignment file to Moss.

## Debug files
File | Description
--- | ---
entities.lst | When the extraction is done, `entities.lst` is a list of entities that were discovered within the Moodle submissions
unzip.log | Records the logs of anything that is unzipped
entity-error.log | Whilst it may not catch all errors, `entity-error.log` will catch any missing files in any entity file when it is preparing the "ForMoss" directory
ForMoss/{FILE}/output.txt | Logs the result of the `moss.pl` upload script. The last word is usually the link to the MOSS results.

## Download Results for Offline Use
Moss results can be saved via the following command:

```bash
wget -e robots=off --wait 1 -rk [MOSS_RESULTS_URL]
```

The first part is to ignore the robots file, the '-rk' says recursively
mirror the page and rewrite all links to local links.  Then you can just
zip up the resulting directory.

## Known Authors
- Dr Michael Wybrow
- Dr Tian Goh
- Moses Wan

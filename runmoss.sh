#!/bin/bash
# vim: ts=4 sw=4 et tw=0 wm=0
#
# Author: Michael Wybrow <Michael.Wybrow@monash.edu>
#
# This script takes a set of Moodle assignment submissions and submits
# particular files to Moss for Plagiarsm chacking.
#
# Usage:
#  1.  Apply for Moss account and download the 'moss.pl' script.  This script
#      needs to be in your path.
#  2.  Update this script with required settings at the top.
#  3.  Go to the Moodle assignment submission, click on "View/grade all
#      submissions.  Under "Grading actions" select "Download all submissions".
#  4.  Unzip the submissions zip file and change into the resulting directory.
#  5.  Run this script in that directory with the argument "extract" to unzip
#      all the sumissions and put the assignments files found within into the
#      'ForMoss' directory.
#  6.  Run this script in that directory with the argument "moss" to submit
#      the submissions for each assignment file to Moss.
#
# Script requires the following command line utilities:
#  cut, unrar, unzip, find, sed, basename, grep
#
# Moss results can be saved via the following command:
#      wget -e robots=off --wait 1 -rk [MOSS_RESULTS_URL]
# The first part is to ignore the robots file, the '-rk' says recursively
# mirror the page and rewrite all links to local links.  Then you can just
# zip up the resulting directory.
#


# Downloading all submissions on Moodle gives you a zip file for each student
# of the form:
#   TEAM 001- Alex_Attic_1234561_assignsubmission_file_Team001.zip
#   TEAM 001- Bella Ball_1234562_assignsubmission_file_Team001.zip
#   TEAM 001- Cam Candle_1234563_assignsubmission_file_Team001.zip
#   TEAM 001- Donna Dish_1234564_assignsubmission_file_Team001.zip
#

# Space separated of submitted file names we would like to upload to Moodle.
ASSIGNMENTFILES="euler2.m fib_clock.m Q1b.m Q2a.m Q2b.m Q2c.m Q2d.m Q3b.m Q3c.m"


# Moodle submission uses groups.
# (Submission will be duplicated for each group member.)
USEGROUPS=false

# Path to directory containing copies of the ASSIGNMENTFILES that were given
# to students to start the project (without trailing slash).
# SKELETONDIR="/mnt/c/Users/yijiew/Documents/ENG1060/Assignment"
SKELETONDIR="${PWD}/template"


# Moss option. The -m option sets the maximum number of times a given passage
# may appear before it is ignored.
MOSSMOPT=20

# Moss option. The -n option determines the number of matching files to show
# in the results.  I reduce this to save archiving.  Mostly the top 25 contain
# the ones worth investigating.
MOSSNOPT=100

ENTITYDIR=""


cleanEntityDir()
{
    if [ -d "$1" ]
    then
        rm -rf "$1"
    fi
    mkdir -p "$1"
}

# Process each of the submissions and extract assignment files to be checked
# and put them into a 'ForMoss' directory with a subdirectory for each
# assignment file.
extract()
{
    # Create ForMoss directory.
    rm -rf "ForMoss/"
    mkdir "ForMoss"

    LASTTEAM=""
    LASTSUBMITTER=""
    COUNT="0"
    rm -rf "entities.lst"
# Moodle submission downloads contains the string "_assignsubmission_file_"
    for DIR in *"_assignsubmission_file_"/
    do
        # Increment count
        COUNT=`expr "$COUNT" + 1`

        # Find the entity.  Will either be the group name if using groups,
        # or the student name and ID.
        SUBMITTER=`echo "$DIR" | cut -f1 -d'_' | sed 's/ //g'`
        if $USEGROUPS
        then
            # Will extract to directory of form 'TeamABC'
            TEAM="$SUBMITTER"

            LASTTEAM="$TEAM"

            ENTITYDIR="$TEAM"
        else
            # Will extract to directory of form 'Alex_Attic_1234561'
            ENTITYDIR="$SUBMITTER"
        fi

        # If this is the first time we're seeing this submitter, clear the
        # output directory.  Save the entity. If not, don't do anything.
        if [ "$SUBMITTER" != "$LASTSUBMITTER" ]; then
            LASTSUBMITTER="$SUBMITTER"
            cleanEntityDir "$ENTITYDIR"
            echo "$ENTITYDIR" >> "entities.lst"

            # Depending on the type of submission, extract it or copy it to
            # the output directory for that submitter.
            if [ -d "$DIR" ]; then
                # Directory, copy to
                FILE=`ls "$DIR"`
                cp "$DIR$FILE" "$ENTITYDIR/"
                if [ "${FILE: -4}" == ".zip" ]; then
                    # ZIP file.
                    echo "${FILE}... zip file"
                    unzip -n -d "$ENTITYDIR" "$ENTITYDIR/$FILE" >> unzip.log
                fi
            fi
        fi

    done

    rebuild
}

rebuild() {

    CURRENTDIR=$PWD
    rm -rf "entity-error.log"
   # For each entity, for each assignment file find if there is a file
    # matching the assignment file and copy to the 'ForMoss' directory,
    # renaming it based on the entity name (team/student).
    while IFS=":" read "ENTITYDIR"
    do
        echo ${ENTITYDIR}...
        (
            cd "${ENTITYDIR}"

            for ASSIGNMENTFILE in $ASSIGNMENTFILES
            do
                mkdir -p "../ForMoss/${ASSIGNMENTFILE}"

                # For a file 'ProjectFile.ext' search for "ProjectFile*.ext".
                EXT="${ASSIGNMENTFILE##*.}"
                BASEFILE=`basename "$ASSIGNMENTFILE" ".${EXT}"`
                LINES=`find . -type f -iname "${BASEFILE}"'*'".${EXT}" | grep -v '__MACOSX'`
                if [ "$LINES" == "" ]
                then
                    # Warn if assignment file not found for this entity.
                    echo "\t\t${ASSIGNMENTFILE} not found" >&2
                    echo "${ENTITYDIR}: ${ASSIGNMENTFILE} not found" >> "${CURRENTDIR}/entity-error.log"
                else
                    # If found, copy and rename.
                    FILENAME="$LINES"
                    EXT="${FILENAME##*.}"
                    cp "$FILENAME" "../ForMoss/${ASSIGNMENTFILE}/${ENTITYDIR}.${EXT}"
                fi
            done
        )

    done < "entities.lst"
}

# Take all of the submissions for each assignment file in the ForMoss directory
# and submit them to Moss, printing the results URL for each.
runmoss()
{
    echo $ASSIGNMENTFILES
    for FILE in $ASSIGNMENTFILES
    do
        ORIGFILE=`find "$SKELETONDIR" -type f -iname "$FILE"`
        EXT="${FILE##*.}"
        TYPE="ascii"
        if [ "$EXT" == "m" ]; then
            TYPE="matlab"
        fi
        (
            cd "ForMoss/${FILE}/"
            echo
            echo "Running MOSS for $FILE..."
            if [ "${ORIGFILE}" != "" ]
            then
                ../../moss.pl -m "${MOSSMOPT}" -n "${MOSSNOPT}" -l "${TYPE}" -b "${ORIGFILE}" *.${EXT} > output.txt
            else
                moss.pl -m "${MOSSMOPT}" -n "${MOSSNOPT}" -l "${TYPE}" *.${EXT} > output.txt
            fi
        )
        echo `tail -1 "ForMoss/${FILE}/output.txt"`
    done
}

if [ "$1" == "extract" ]
then
  extract
elif [ "$1" == "moss" ]
then
  runmoss
elif [ "$1" == "rebuild" ]
then
  rebuild
fi
